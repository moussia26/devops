terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.64"
    }
  }
}


# Création d'une instance AWS
provider "aws" {
  secret_key = "QjhzpK1hmV5mj3hTcu8eHLC69CIme1qsXi9jXC/i"
  access_key = "AKIAXRRLPHDMEGWUULSJ"
  region     = "eu-west-1"
}
# creation d'un vpc
resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
}

# creation d'un internet gateway
resource "aws_internet_gateway" "my_ig" {
  vpc_id = aws_vpc.my_vpc.id
}
resource "aws_route_table" "my_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_ig.id
  }
}


resource "aws_subnet" "my_subnet" {
  availability_zone = "eu-west-1a"
  cidr_block        = "10.0.1.0/24"
  vpc_id            = aws_vpc.my_vpc.id

  tags = {
    Projet = "DevOps"
  }
}


resource "aws_route_table_association" "my_route_table_association" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.my_route_table.id
}




resource "aws_security_group" "my_security_group" {
  name_prefix = "AKIAXRRLPHDMEGWUULSJ"
  vpc_id      = aws_vpc.my_vpc.id

  tags = {
    Projet = "Devops"
  }
}

resource "aws_security_group_rule" "my_security_group_rule_out_http" {
  type              = "egress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}



resource "aws_security_group_rule" "my_security_group_rule_out_https" {
  type              = "egress"
  from_port         = "443"
  to_port           = "443"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}


resource "aws_security_group_rule" "my_security_group_rule_http_in" {
  type              = "egress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}


resource "aws_instance" "my_instance" {
  ami                         = data.aws_ami.ubuntu-linux-1804.id
  subnet_id                   = aws_subnet.my_subnet.id
  instance_type               = "t2.micro"
  associate_public_ip_address = "true"
  vpc_security_group_ids      = [aws_security_group.my_security_group.id]
  key_name                    = aws_key_pair.ssh-key.key_name
}
/
resource "aws_key_pair" "ssh-key" {
  key_name   = "ssh_key-1"
  public_key = var.SSH_KEY
}

output "ip" {
  value = aws_instance.my_instance.public_ip
}