# Création d'une instance AWS
provider "aws" {
  secret_key = "AKIAXRRLPHDMEGWUULSJ"
  access_key = "QjhzpK1hmV5mj3hTcu8eHLC69CIme1qsXi9jXC"
  region     = "eu-west-1"
}
  # creation d'un vpc
 resource "aws_vpc" "my_vpc" {
    cidr_block = "10.0.0.0/16"
  }

  # creation d'un internet gateway
resource "aws_internet_gateway" "my_ig" {
    vpc_id = "l'id du vpc que vous venez de créé"
  }
resource "aws_instance" "web" {
  ami                         = "ami-02297540444991cc0"
  subnet_id                   = aws_instance
  instance_type               = "t2.micro"
  vpc_security_group_ids      = "l’id du security group créé précédemment"
  associate_public_ip_address = "true"
  Name                        = "moussia"
  User                        = "AKIAXRRLPHDMEGWUULSJ"
  TP                          = "projet"

  tags = {
    Name      = "mottal.moussia"
    Formation = "terraform"
    TP        = "Projet"
  }


  my_route_table = {
    vpc_id = "lid du vpc que vous venez de créé"
    route = {
      cidr_block = "0.0.0.0/0"
      gateway_id = "my_ig"
    }
  }
  my_subnet = {
    availability_zone = "eu-west-1a"
    cidr_block        = "10.0.1.0/24"
    vpc_id            = "vpc-0856032ce82ce8221"
    Name              = "moussia"
    User              = "AKIAXRRLPHDMEGWUULSJ"
    TP                = "Projet"
  }
  my_route_table_association = {
    subnet_id      = "lid du subnet créé précédemmen"
    route_table_id = "rtb-0418a191a893f8d1f"
  }
  my_security_group = {
    name_prefix = "AKIAXRRLPHDMEGWUULSJ"
    vpc_id      = "vpc-0856032ce82ce8221"
    Name        = "moussia"
    User        = "AKIAXRRLPHDMEGWUULSJ"
    TP          = "Projet"
  }
  my_security_group_rule_out_http = {
    type              = "egress"
    from_port         = "443"
    to_port           = "80"
    protocol          = "tcp"
    cidr_blocks       = "0.0.0.0/0"
    security_group_id = "id du security group créé précédemment"
  }

  my_security_group_rule_out_https = {
    type              = "egress"
    from_port         = "443"
    to_port           = "443"
    protocol          = "tcp"
    cidr_blocks       = "0.0.0.0/0"
    security_group_id = "id du security group créé précédemment"
  }

  my_security_group_rule_http_in = {
    type              = "egress"
    from_port         = "80"
    to_port           = "80"
    protocol          = "tcp"
    cidr_blocks       = "0.0.0.0/0"
    security_group_id = "id du security group créé précédemment"
  }

}