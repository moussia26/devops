# Création d'une instance AWS
resource "aws_instance" "web" {
  ami           = "ami-0e3f7a235a05f8e99"
  instance_type = "t2.micro"

  tags = {
    Name      = "mottal.moussia"
    Formation = "terraform"
    TP        = "Projet"
  }
  resource "aws_vpc" "my_vpc" {
    cidr_block = "192.168.1.0/24"

    enable_dns_hostnames = true
    enable_dns_support   = true

  }
}