# Création d'une instance AWS
resource "aws_instance" "web" {
  ami           = "ami-0e3f7a235a05f8e99"
  instance_type = "t2.micro"

  tags = {
    Name      = "mottal.moussia"
    Formation = "terraform"
    TP        = "Projet"
  }
  my_vpc = {
    cidr_block = "10.0.0.0/16"
  }
}